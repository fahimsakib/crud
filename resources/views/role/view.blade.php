@extends('master')

@section('content')
<section class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="{{url('role')}}" class="btn btn-secondary">back</a>
            </div>
            <div class="col-md-12">
                <p><b>Role Name:</b> {{$role->role_name}}</p>
            </div>
        </div>
    </div>
</section>
@endsection
