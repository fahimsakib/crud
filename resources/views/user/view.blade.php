@extends('master')

@section('content')

<section class="container-fluid">
    <div class="container row justify-content-center">
        <div class="card col-md-5 py-2">
            <img src="{{asset(FOLDER_PATH.USER_PHOTO.$user->photo)}}"
                class="card-img-top rounded-circle mx-auto d-block border border-primary"
                style="width: 50%; height: 220px" alt="{{$user->name}}">
            <div class="card-body">
                <h2 class="card-title text-center">{{$user->name}}</h2>
                <h4 class="text-center">{{$user->role->role_name}}</h4>
                <div class="col-md-12 mt-3">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <th>Email:</th>
                                <td>{{$user->email}}</td>
                            </tr>
                            <tr>
                                <th>Mobile:</th>
                                <td>{{$user->mobile}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
