@extends('master')

@section('content')
<section class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="{{url('role')}}" class="btn btn-secondary">back</a>
            </div>
            <div class="col-md-12">
                <form action="{{url('role/store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="">Role Name</label>
                        <input type="text" class="form-control @error('role_name') is-invalid @enderror" name="role_name" id="role_name">
                        @error('role_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
