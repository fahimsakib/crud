<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Traits\UploadFile;

class UserController extends Controller
{
    use UploadFile;
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::OrderBY('id','desc')->paginate(2);
        return view('user.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('user.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name'                  => 'required|string',
            'email'                 => 'required|email|unique:users',
            'mobile'                => 'required|numeric|unique:users',
            'photo'                 => 'required|image|mimes:png,jpg,jpeg',
            'role_id'               => 'required|numeric',
            'password'              => 'required|string|confirmed|min:8',
            'password_confirmation' => 'required|string|min:8',
        ]);
        
        $photo = $this->upload_file($request->photo,USER_PHOTO);
        $result = User::create([
            'name'     => $request->name,
            'email'    => $request->email,
            'mobile'   => $request->mobile,
            'photo'    => $photo,
            'role_id'  => $request->role_id,
            'password' => Hash::make($request->password),
        ]);
        if($result)
        {
            return redirect('user')->with('success','Data Saved Successfully');
        }else{
            $this->delete_file($photo,USER_PHOTO);
            return redirect('user')->with('error','Data can not save');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $user = User::find($id);
       return view('user.view',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();
        $user = User::find($id);
        return view('user.edit',compact('user','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'name'                  => 'required|string',
            'email'                 => 'required|email|unique:users,email,'.$id,
            'mobile'                => 'required|numeric|unique:users,mobile,'.$id,
            'photo'                 => 'image|mimes:png,jpg,jpeg',
            'role_id'               => 'required|numeric',
        ]);
        $user = User::find($id);
            $user->name     = $request->name;
            $user->email    = $request->email;
            $user->mobile   = $request->mobile;
            if(!empty($request->photo)){
                $this->delete_file($user->photo,USER_PHOTO);
                $user->photo = $this->upload_file($request->photo,USER_PHOTO);
            }

            $user->role_id  = $request->role_id;
        if($user->update())
        {
            return redirect('user')->with('success','Data updated Successfully');
        }else{
            return redirect('user/edit/'.$id)->with('error','Data can not update');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::find($id);
        if(!empty($data->photo)){
            $this->delete_file($data->photo,USER_PHOTO);
        }
        if($data->delete())
        {
            return redirect('user')->with('success','Data deleted Successfully');
        }else{
            return redirect('user')->with('error','Data can not delete');
        }
    }
}
