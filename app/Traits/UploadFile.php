<?php
namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait UploadFile
{
    public function upload_file(UploadedFile $file, $folder = null, $file_name = null)
    {
        $disk = 'public';
        if (!Storage::directories($disk.'/'.$folder)){
            Storage::makeDirectory($disk.'/'.$folder,0777,true);
        }
        $filenameWithExt = $file->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $file->getClientOriginalExtension();

        $fileNameToStore = !is_null($file_name) ? $file_name.'.'.$extension : $filename.uniqid().'.'.$extension;
        $file->storeAs($folder,$fileNameToStore,$disk);
        return $fileNameToStore;
    }

    public function delete_file($filename,$folder,$disk = 'public')
    {
        if(Storage::exists($disk.'/'.$folder.$filename))
        {
            Storage::disk($disk)->delete($folder.$filename);
            return TRUE;
        }
        return false;
    }
}