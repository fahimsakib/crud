<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/','HomeController@index');
// Route::get('/create','HomeController@create');

// use Illuminate\Routing\Route;

// Route::get('/', function () {
//     return view('welcome');
// });
// Auth::routes();


Route::get('/','Auth\LoginController@showLoginForm');
Route::post('login','Auth\LoginController@login')->name('login');

Route::group(['middleware' => 'auth'],function () {
    Route::post('logout','Auth\LoginController@logout')->name('logout');
    Route::get('dashboard', 'HomeController@index');

    Route::get('role','RoleController@index');
    Route::get('role/add','RoleController@create');
    Route::post('role/store','RoleController@store');
    Route::get('role/edit/{id}','RoleController@edit');
    Route::get('role/view/{id}','RoleController@show');
    Route::put('role/update/{id}','RoleController@update');
    Route::get('role/delete/{id}','RoleController@destroy');

    Route::resource('user','UserController')->except(['edit','show','destroy']);
    Route::get('user/edit/{id}','UserController@edit');
    Route::get('user/show/{id}','UserController@show');
    Route::get('user/delete/{id}','UserController@destroy');
});

