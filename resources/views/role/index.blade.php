@extends('master')

@section('content')
    <section class="container-fluid">
        <div class="container">
            @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{session('success')}}
            </div>
            @endif
            @if (session('error'))
            <div class="alert alert-danger" role="alert">
                {{session('error')}}
            </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                <a href="{{url('role/add')}}" type="button" class="btn btn-success m-2">Add Role</a>
                </div>
                <div class="col-md 12">
                    <div class="table-responsive">
                        <table class="table table-bordered teble-striped table-hovered">
                            <thead>
                                <th>Sl</th>
                                <th>Role Name</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @php
                                    $i=1;
                                @endphp
                                @foreach ($roles as $item)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$item->role_name}}</td>
                                    <td>
                                        <div class="btn-group open">
                                            <button data-toggle="dropdown"
                                            class="btn btn-outline btn-primary dropdown-toggle"
                                            aria-expanded="true">Action</button>
                                                <ul class="dropdown-menu pull-right">
                                                    <li class="px-2 py-1"><a href="{{url('role/edit',$item->id)}}">Edit</a></li>
                                                    <li class="px-2 py-1"><a href="{{url('role/view',$item->id)}}">View</a></li>
                                                    <li class="px-2 py-1"><a href="{{url('role/delete',$item->id)}}">Delete</a></li>
                                                </ul> 
                                        </div>
                                    </td>
                                </tr> 
                                @endforeach
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection