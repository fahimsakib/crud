<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use DB; // for query builder

class RoleController extends Controller
{
   
    public function index()
    {
        $roles = Role::all();
        // $roles = DB::table('roles')->get(); //query builder
        return view('role.index',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('role.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'role_name' => 'required|unique:roles|max:50'
        ]);

        // $role = new Role();
        // $role->role_name = $request->role_name;
        // if($role->save()){
        //     return redirect('role')->with('success','Data saved Successfully');
        // }else{
        //     return redirect('role')->with('error','Data can not save');
        // }

        //or

        // $role =Role::create([
        //     'role_name' => $request->role_name
        // ]);
        // if($role)
        // {
        //     return redirect('role')->with('success','Data saved Successfully');
        // }else{
        //     return redirect('role')->with('error','Data can not save');
        // }

        // or

        $data = collect($request)->except('_token');
        $role = Role::create($data->all());
        if($role)
        {
            return redirect('role')->with('success','Data saved Successfully');
        }else{
            return redirect('role')->with('error','Data can not save');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        return view('role.view',compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        return view('role.edit',compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $validatedData = $request->validate([
            'role_name' => 'required|max:50|unique:roles,role_name,'.$id,
        ]);

        // $role = Role::find($id);
        // $role->role_name = $request->role_name;
        // if($role->update())
        // {
        //     return redirect('role')->with('success','Data updated Successfully');
        // }else{
        //     return redirect('role')->with('error','Data can not update');
        // }

        $role = Role::find($id)->update([
            'role_name' => $request->role_name
        ]);
        if($role)
        {
            return redirect('role')->with('success','Data updated Successfully');
        }else{
            return redirect('role')->with('error','Data can not update');
        }

    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Role::find($id)->delete();
        if($result)
        {
            return redirect('role')->with('success','Data deleted Successfully');
        }else{
            return redirect('role')->with('error','Data can not delete');
        }
    }
}
