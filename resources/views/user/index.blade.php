@extends('master')

@section('content')

<section class="container-fluid">
    <div class="container">
        <div class="text-right py-2">
            <a href="{{url('user/create')}}" class="btn btn-success">Add New User</a>
        </div>
        <div class="row">
            @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{session('success')}}
            </div>
            @endif
            @if (session('error'))
            <div class="alert alert-danger" role="alert">
                {{session('error')}}
            </div>
            @endif
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hovered">
                        <thead>
                            <th>SL</th>
                            <th>Name</th>
                            <th>Role</th>
                            <th>Eamil</th>
                            <th>Mobile</th>
                            <th>Photo</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @if (!empty($data))
                            @php
                            $i=1;
                            @endphp
                            @foreach ($data as $item)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->role->role_name}}</td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->mobile}}</td>
                                <td>
                                    <img src="{{asset(FOLDER_PATH.USER_PHOTO.$item->photo)}}" alt="{{$item->name}}"
                                        style="width:70px; height:70px;">
                                </td>
                                <td>
                                    <div class="btn-group open">
                                        <button data-toggle="dropdown"
                                            class="btn btn-outline btn-primary dropdown-toggle"
                                            aria-expanded="true">Action</button>
                                        <ul class="dropdown-menu pull-right">
                                            <li class="px-2 py-1"><a href="{{url('user/edit',$item->id)}}">Edit</a></li>
                                            <li class="px-2 py-1"><a href="{{url('user/show',$item->id)}}">View</a></li>
                                            <li class="px-2 py-1"><a href="{{url('user/delete',$item->id)}}">Delete</a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    {{$data->links() }}
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
