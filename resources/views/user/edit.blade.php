@extends('master')

@section('content')

<div class="container content pt-5 " id="content">
    <div class="col-md-12">
        <a href="{{url('user')}}" class="btn btn-secondary">back</a>
    </div>
    <h2 class="text-center">User Registration</h2>
    <div class="row justify-content-center">
        <form action="{{url('user',$user->id)}}" class="form-horizontal pt-5 pb-5 col-md-6" method="post"
            enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="name">Name</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" value="{{$user->name}}">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-12">
                    <label for="role_id">Role</label>
                    <select class="form-control @error('role_id') is-invalid @enderror" name="role_id" id="role_id">
                        @if (!empty($roles))
                        @foreach ($roles as $role)
                        <option @if($role->id == $user->role_id) {{'selected'}} @endif value="{{$role->id}}">{{$role->role_name}}</option>
                        @endforeach
                        @endif
                    </select>
                    @error('role_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-12">
                    <label for="email">Email</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                        id="eamil" value="{{$user->email}}">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-12">
                    <label for="mobile">Mobile</label>
                    <input type="text" class="form-control @error('mobile') is-invalid @enderror" name="mobile"
                        id="mobile" value="{{$user->mobile}}">
                    @error('mobile')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-12">
                    <label for="photo">Photo</label>
                    <input type="file" class="form-control @error('photo') is-invalid @enderror" name="photo"
                        id="photo" value="{{$user->photo}}">
                    @error('photo')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <img src="{{asset(FOLDER_PATH.USER_PHOTO.$user->photo)}}"
                    class="card-img-top mx-auto d-block"
                    style="width: 50%; " alt="{{$user->name}}">
                </div>
            </div>
            <button type="submit" class="btn btn-primary" name="save">Update</button>
            <button type="reset" class="btn btn-danger">Cancel</button>
        </form>
    </div>
</div>

</section>
@endsection
